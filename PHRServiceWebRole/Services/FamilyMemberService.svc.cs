﻿using System;
using System.Data.SqlClient;
using System.Collections.Generic;

namespace PHRServiceWebRole
{
    public class FamilyMemberService : IFamilyMemberService
    {
        public PersonArrayMessage getAllFamilyMembers(IdMessage idUser)
        {
            SqlConnection conn = new SqlConnection("Data Source=kx1lehq9ru.database.windows.net;Initial Catalog=dsacwphrdatabase;Persist Security Info=True;User ID=nimda;Password=ABCabc123#");
            conn.Open();

            SqlCommand command = conn.CreateCommand();
            command.CommandText = "SELECT * FROM Person WHERE (IdUser = " + idUser.Id + ")";
            SqlDataReader reader = command.ExecuteReader();

            List<Person> personList = new List<Person>();

            while (reader.Read())
            {
                Person person = new Person();
                person.Address = reader["Address"].ToString().Trim();

                if (!reader.IsDBNull(reader.GetOrdinal("DateOfBirth")))
                {
                    person.DateOfBirth = reader.GetDateTime(reader.GetOrdinal("DateOfBirth"));
                }                
                person.Email = reader["Email"].ToString().Trim();
                person.Gender = (Gender)reader.GetInt32(reader.GetOrdinal("Gender"));
                person.Id = reader.GetInt32(reader.GetOrdinal("Id"));
                person.IdUser = reader.GetInt32(reader.GetOrdinal("IdUser"));
                person.Name = reader["Name"].ToString().Trim();
                personList.Add(person);
            }

            conn.Close();
            
            PersonArrayMessage personArrayMessage = new PersonArrayMessage();
            personArrayMessage.Timestamp = DateTime.Now;
            personArrayMessage.PersonArray = personList.ToArray();
            return personArrayMessage;
        }

        public BooleanMessage addFamilyMember(PersonMessage personMessage)
        {
            SqlConnection conn = new SqlConnection("Data Source=kx1lehq9ru.database.windows.net;Initial Catalog=dsacwphrdatabase;Persist Security Info=True;User ID=nimda;Password=ABCabc123#");
            conn.Open();

            SqlCommand command = conn.CreateCommand();

            string commandTxt1 = "INSERT INTO Person (Name, IdUser, Address, DateOfBirth, Gender, Email) VALUES (N'"
                + personMessage.Person.Name + "', "
                + personMessage.Person.IdUser + ", N'"
                + personMessage.Person.Address;

            string commandTxt2;
            if (personMessage.Person.DateOfBirth == DateTime.MinValue)
            {
                commandTxt2 = "', NULL";
            }
            else
            {
                commandTxt2 = "', CONVERT(DATETIME, '"
                    + String.Format("{0:yyyy-M-d HH:mm:ss}", personMessage.Person.DateOfBirth) + "', 102)";
            }
            string commandTxt3 = ", "
                + (int)personMessage.Person.Gender + ", N'"
                + personMessage.Person.Email + "')";

            command.CommandText = commandTxt1 + commandTxt2 + commandTxt3;

            BooleanMessage booleanMessage = new BooleanMessage();
            booleanMessage.Timestamp = DateTime.Now;
            booleanMessage.Value = command.ExecuteNonQuery() != 0;

            conn.Close();

            return booleanMessage;
        }

        public BooleanMessage removeFamilyMember(PersonMessage personMessage)
        {
            SqlConnection conn = new SqlConnection("Data Source=kx1lehq9ru.database.windows.net;Initial Catalog=dsacwphrdatabase;Persist Security Info=True;User ID=nimda;Password=ABCabc123#");
            conn.Open();

            SqlCommand command = conn.CreateCommand();
            command.CommandText = "DELETE FROM Person WHERE (Id = " + personMessage.Person.Id + ")";

            BooleanMessage booleanMessage = new BooleanMessage();
            booleanMessage.Timestamp = DateTime.Now;
            booleanMessage.Value = command.ExecuteNonQuery() != 0;

            conn.Close();

            return booleanMessage;
        }

        public BooleanMessage editFamilyMember(PersonMessage personMessage)
        {
            SqlConnection conn = new SqlConnection("Data Source=kx1lehq9ru.database.windows.net;Initial Catalog=dsacwphrdatabase;Persist Security Info=True;User ID=nimda;Password=ABCabc123#");
            conn.Open();

            SqlCommand command = conn.CreateCommand();

            string commandTxt1 = "UPDATE Person SET Name = N'" 
                + personMessage.Person.Name + "', Address = N'"
                + personMessage.Person.Address;

            string commandTxt2;
            if (personMessage.Person.DateOfBirth == DateTime.MinValue)
            {
                commandTxt2 = "', DateOfBirth = NULL";
            }
            else 
            {
                commandTxt2 = "', DateOfBirth = CONVERT(DATETIME, '"
                + String.Format("{0:yyyy-M-d HH:mm:ss}", personMessage.Person.DateOfBirth) + "', 102)";            
            }

            string commandTxt3 = ", Gender = "
                + (int)personMessage.Person.Gender + ", Email = N'"
                + personMessage.Person.Email + "' WHERE (Id = "
                + personMessage.Person.Id + ")";

            command.CommandText = commandTxt1 + commandTxt2 + commandTxt3;
            BooleanMessage booleanMessage = new BooleanMessage();
            booleanMessage.Timestamp = DateTime.Now;
            booleanMessage.Value = command.ExecuteNonQuery() != 0;

            conn.Close();

            return booleanMessage;
        }
    }
}
