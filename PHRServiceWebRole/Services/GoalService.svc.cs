﻿using System;
using System.Data.SqlClient;
using System.Collections.Generic;

namespace PHRServiceWebRole
{
    public class GoalService : IGoalService
    {
        public GoalArrayMessage getAllGoals(IdMessage idUser)
        {
            SqlConnection conn = new SqlConnection("Data Source=kx1lehq9ru.database.windows.net;Initial Catalog=dsacwphrdatabase;Persist Security Info=True;User ID=nimda;Password=ABCabc123#");
            conn.Open();

            SqlCommand command = conn.CreateCommand();
            command.CommandText = "SELECT * FROM Goal WHERE (IdUser = " + idUser.Id + ")";
            SqlDataReader reader = command.ExecuteReader();

            List<Goal> goalList = new List<Goal>();

            while (reader.Read())
            {
                Goal goal = new Goal();
                goal.Caption = reader["Caption"].ToString().Trim();
                goal.CurrentProgress = reader.GetInt32(reader.GetOrdinal("CurrentProgress"));
                goal.Description = reader["Description"].ToString().Trim();
                goal.Id = reader.GetInt32(reader.GetOrdinal("Id"));
                goal.IdUser = reader.GetInt32(reader.GetOrdinal("IdUser"));
                goalList.Add(goal);
            }

            conn.Close();

            GoalArrayMessage goalArrayMessage = new GoalArrayMessage();
            goalArrayMessage.Timestamp = DateTime.Now;
            goalArrayMessage.GoalArray = goalList.ToArray();
            return goalArrayMessage;
        }

        public BooleanMessage addGoal(GoalMessage goalMessage)
        {
            SqlConnection conn = new SqlConnection("Data Source=kx1lehq9ru.database.windows.net;Initial Catalog=dsacwphrdatabase;Persist Security Info=True;User ID=nimda;Password=ABCabc123#");
            conn.Open();

            SqlCommand command = conn.CreateCommand();
            command.CommandText = "INSERT INTO Goal (IdUser, Caption, Description, CurrentProgress) VALUES ("
                + goalMessage.Goal.IdUser + ", N'"
                + goalMessage.Goal.Caption + "', N'"
                + goalMessage.Goal.Description + "', 0)";
            command.ExecuteNonQuery();

            command.CommandText = "SELECT MAX(Id) AS idLastGoal FROM Goal";
            int idLastGoal = (int)command.ExecuteScalar();

            command.CommandText = "INSERT INTO GoalProgress (IdGoal, Date, Progress) VALUES ("
                + idLastGoal + ", CONVERT(DATETIME, '"
                + String.Format("{0:yyyy-M-d HH:mm:ss}", DateTime.Now) + "', 102), "
                + "0)";
            
            BooleanMessage booleanMessage = new BooleanMessage();
            booleanMessage.Timestamp = DateTime.Now;
            booleanMessage.Value = command.ExecuteNonQuery() != 0;
            conn.Close();

            return booleanMessage;
        }

        public BooleanMessage removeGoal(GoalMessage goalMessage)
        {
            SqlConnection conn = new SqlConnection("Data Source=kx1lehq9ru.database.windows.net;Initial Catalog=dsacwphrdatabase;Persist Security Info=True;User ID=nimda;Password=ABCabc123#");
            conn.Open();

            SqlCommand command = conn.CreateCommand();
            command.CommandText = "DELETE FROM Goal WHERE (Id = " + goalMessage.Goal.Id + ")";
            int result1 = command.ExecuteNonQuery();
            command.CommandText = "DELETE FROM GoalProgress WHERE (IdGoal = " + goalMessage.Goal.Id + ")";
            int result2 = command.ExecuteNonQuery();
            
            BooleanMessage booleanMessage = new BooleanMessage();
            booleanMessage.Timestamp = DateTime.Now;
            booleanMessage.Value =  Convert.ToBoolean(result1) && Convert.ToBoolean(result2);

            conn.Close();

            return booleanMessage;
        }

        public BooleanMessage editGoal(GoalMessage goalMessage)
        {
            SqlConnection conn = new SqlConnection("Data Source=kx1lehq9ru.database.windows.net;Initial Catalog=dsacwphrdatabase;Persist Security Info=True;User ID=nimda;Password=ABCabc123#");
            conn.Open();

            SqlCommand command = conn.CreateCommand();
            command.CommandText = "INSERT INTO GoalProgress (IdGoal, Date, Progress) VALUES ("
                + goalMessage.Goal.Id + ", CONVERT(DATETIME, '"
                + String.Format("{0:yyyy-M-d HH:mm:ss}", DateTime.Now) + "', 102), "
                + goalMessage.Goal.CurrentProgress + ")";
            int result1 = command.ExecuteNonQuery();

            command.CommandText = "UPDATE Goal SET CurrentProgress = " 
                + goalMessage.Goal.CurrentProgress + ", Caption = N'"
                + goalMessage.Goal.Caption + "', Description = N'"
                + goalMessage.Goal.Description + "' WHERE (Id = "
                + goalMessage.Goal.Id + ")";
            int result2 = command.ExecuteNonQuery();

            BooleanMessage booleanMessage = new BooleanMessage();
            booleanMessage.Timestamp = DateTime.Now;
            booleanMessage.Value = Convert.ToBoolean(result1) && Convert.ToBoolean(result2);

            conn.Close();

            return booleanMessage;
        }

        public ProgressMapMessage getAllProgress(GoalMessage goalMessage)
        {
            SqlConnection conn = new SqlConnection("Data Source=kx1lehq9ru.database.windows.net;Initial Catalog=dsacwphrdatabase;Persist Security Info=True;User ID=nimda;Password=ABCabc123#");
            conn.Open();

            SqlCommand command = conn.CreateCommand();
            command.CommandText = "SELECT * FROM GoalProgress WHERE (IdGoal = " + goalMessage.Goal.Id + ")";
            SqlDataReader reader = command.ExecuteReader();

            Dictionary<DateTime, int> progressMap = new Dictionary<DateTime, int>();

            while (reader.Read())
            {
                progressMap.Add(reader.GetDateTime(reader.GetOrdinal("Date")), reader.GetInt32(reader.GetOrdinal("Progress")));
            }

            conn.Close();

            ProgressMapMessage progressMapMessage = new ProgressMapMessage();
            progressMapMessage.Timestamp = DateTime.Now;
            progressMapMessage.ProgressMap = progressMap;
            return progressMapMessage;
        }
    }
}
