﻿using System.ServiceModel;

namespace PHRServiceWebRole
{
    /// <summary>
    /// The interface ITaskService defines the methods the service offers
    /// to manage user's tasks.
    /// </summary>
    [ServiceContract]
    public interface ITaskService
    {
        /// <summary>
        /// This method returns a TaskArrayMessage which TaskArray value contains
        /// all the tasks for the given user.
        /// </summary>
        /// <param name="idUser">The IdMessage containing the user Id</param>
        /// <returns>The TaskArrayMessage</returns>
        [OperationContract]
        TaskArrayMessage getAllTasks(IdMessage idUser);

        /// <summary>
        /// This method adds a task into the database. It returns a BooleanMessage which boolean value 
        /// is true if the given task has been succesfully added or false if an error occured.
        /// </summary>
        /// <param name="taskMessage">The taskMessage containing the task to add</param>
        /// <returns>The BooleanMessage</returns>
        [OperationContract]
        BooleanMessage addTask(TaskMessage taskMessage);

        /// <summary>
        /// This method removes a task from the database. It returns a BooleanMessage which boolean value 
        /// is true if the given task has been succesfully removed or false if an error occured.
        /// </summary>
        /// <param name="taskMessage">The taskMessage containing the task to remove</param>
        /// <returns>The BooleanMessage</returns>
        [OperationContract]
        BooleanMessage removeTask(TaskMessage taskMessage);

        /// <summary>
        /// This method edits a task from the database. It returns a BooleanMessage which boolean value 
        /// is true if the given task has been succesfully edited or false if an error occured.
        /// </summary>
        /// <param name="taskMessage">The taskMessage containing the task to edit</param>
        /// <returns>The BooleanMessage</returns>
        [OperationContract]
        BooleanMessage editTask(TaskMessage taskMessage);
    }
}
