﻿using System;
using System.Data.SqlClient;

namespace PHRServiceWebRole
{
    public class LoginService : ILoginService
    {
        public IdMessage login(UserMessage userMessage)
        {
            SqlConnection conn = new SqlConnection("Data Source=kx1lehq9ru.database.windows.net;Initial Catalog=dsacwphrdatabase;Persist Security Info=True;User ID=nimda;Password=ABCabc123#");
            conn.Open();

            SqlCommand command = conn.CreateCommand();
            command.CommandText = "SELECT Id FROM [User] WHERE (Username = N'" + userMessage.User.Username + "') AND (Password = N'" + userMessage.User.Password + "')";
            SqlDataReader reader = command.ExecuteReader();

            IdMessage idMessage = new IdMessage();
            idMessage.Timestamp = DateTime.Now;
            idMessage.Id = -1;

            if (reader.Read())
            {
                idMessage.Id = int.Parse(reader["Id"].ToString().Trim());
            }

            conn.Close();

            return idMessage;
        }

        public IdMessage register(UserMessage userMessage)
        {
            SqlConnection conn = new SqlConnection("Data Source=kx1lehq9ru.database.windows.net;Initial Catalog=dsacwphrdatabase;Persist Security Info=True;User ID=nimda;Password=ABCabc123#");
            conn.Open();

            SqlCommand command = conn.CreateCommand();

            IdMessage idMessage = new IdMessage();
            idMessage.Timestamp = DateTime.Now;

            command.CommandText = "INSERT INTO [User] (Username, Password) VALUES (N'" + userMessage.User.Username + "', N'" + userMessage.User.Password + "')";
            int result1 = command.ExecuteNonQuery();

            command.CommandText = "SELECT MAX(Id) AS idLastUser FROM [User]";
            int idLastUser = (int)command.ExecuteScalar();

            string commandTxt1 = "INSERT INTO Person (Name, IdUser, Address, DateOfBirth, Gender, Email) VALUES (N'"
                + userMessage.User.Name + "', "
                + idLastUser + ", N'"
                + userMessage.User.Address;

            string commandTxt2;
            if (userMessage.User.DateOfBirth == DateTime.MinValue)
            {
                commandTxt2 = "', NULL";
            }
            else
            {
                commandTxt2 = "', CONVERT(DATETIME, '"
                    + String.Format("{0:yyyy-M-d HH:mm:ss}", userMessage.User.DateOfBirth) + "', 102)";
            }
            string commandTxt3 = ", "
                + (int)userMessage.User.Gender + ", N'"
                + userMessage.User.Email + "')";

            command.CommandText = commandTxt1 + commandTxt2 + commandTxt3;
            command.ExecuteNonQuery();

            idMessage.Id = idLastUser;

            conn.Close();

            return idMessage;
        }

        public IdMessage getUserLoggedInId(UserMessage userMessage)
        {
            SqlConnection conn = new SqlConnection("Data Source=kx1lehq9ru.database.windows.net;Initial Catalog=dsacwphrdatabase;Persist Security Info=True;User ID=nimda;Password=ABCabc123#");
            conn.Open();

            SqlCommand command = conn.CreateCommand();
            command.CommandText = "SELECT Id FROM [User] WHERE (Username = N'" + userMessage.User.Username + "')";
            SqlDataReader reader = command.ExecuteReader();

            IdMessage idMessage = new IdMessage();
            idMessage.Timestamp = DateTime.Now;
            idMessage.Id = -1;

            if (reader.Read())
            {
                idMessage.Id = reader.GetInt32(reader.GetOrdinal("Id"));
            }

            conn.Close();

            return idMessage;
        }
    }
}
