﻿using System.ServiceModel;

namespace PHRServiceWebRole
{
    /// <summary>
    /// The interface IGoalService defines the methods the service offers
    /// to manage user's goals.
    /// </summary>
    [ServiceContract]
    public interface IGoalService
    {
        /// <summary>
        /// This method returns a GoalArrayMessage which GoalArray value contains
        /// all the goals for the given user.
        /// </summary>
        /// <param name="idUser">The IdMessage containing the user Id</param>
        /// <returns>The GoalArrayMessage</returns>
        [OperationContract]
        GoalArrayMessage getAllGoals(IdMessage idUser);

        /// <summary>
        /// This method adds a goal into the database. It returns a BooleanMessage which boolean value 
        /// is true if the given goal has been succesfully added or false if an error occured.
        /// </summary>
        /// <param name="goalMessage">The GoalMessage containing the goal to add</param>
        /// <returns>The BooleanMessage</returns>
        [OperationContract]
        BooleanMessage addGoal(GoalMessage goalMessage);

        /// <summary>
        /// This method removes a goal from the database. It returns a BooleanMessage which boolean value 
        /// is true if the given goal has been succesfully removed or false if an error occured.
        /// </summary>
        /// <param name="goalMessage">The GoalMessage containing the goal to remove</param>
        /// <returns>The BooleanMessage</returns>
        [OperationContract]
        BooleanMessage removeGoal(GoalMessage goalMessage);

        /// <summary>
        /// This method edits a goal from the database. It returns a BooleanMessage which boolean value 
        /// is true if the given goal has been succesfully edited or false if an error occured.
        /// </summary>
        /// <param name="goalMessage">The GoalMessage containing the goal to edit</param>
        /// <returns>The BooleanMessage</returns>
        [OperationContract]
        BooleanMessage editGoal(GoalMessage goalMessage);

        /// <summary>
        /// This method returns a ProgressMapMessage which ProgressMap value contains
        /// all the progress for the given goal.
        /// </summary>
        /// <param name="goalMessage">The GoalMessage containing the goal Id</param>
        /// <returns>The ProgressMapMessage</returns>
        [OperationContract]
        ProgressMapMessage getAllProgress(GoalMessage goalMessage);
    }
}
