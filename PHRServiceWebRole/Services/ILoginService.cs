﻿using System.ServiceModel;

namespace PHRServiceWebRole
{
    /// <summary>
    /// The interface ILoginService defines the methods the service offers
    /// about login and registration.
    /// </summary>
    [ServiceContract]
    public interface ILoginService
    {
        /// <summary>
        /// This method returns an IdMessage which Id value is the Id of the user in the database
        /// or -1 if the user was not found.
        /// </summary>
        /// <param name="userMessage">The userMessage containing the username and the password</param>
        /// <returns>The IdMessage</returns>
        [OperationContract]
        IdMessage login(UserMessage userMessage);

        /// <summary>
        /// This method returns an IdMessage which Id value is the Id of the inserted user in the database
        /// or -1 if the insertion failed.
        /// </summary>
        /// <param name="userMessage">The userMessage containing the registration information</param>
        /// <returns>The IdMessage</returns>
        [OperationContract]
        IdMessage register(UserMessage userMessage);

        /// <summary>
        /// This method returns an IdMessage which Id value is the ID of the user with the corresponding
        /// username in the database or -1 if the user was not found.
        /// </summary>
        /// <param name="userMessage">The userMessage containing the username</param>
        /// <returns>The IdMessage</returns>
        [OperationContract]
        IdMessage getUserLoggedInId(UserMessage userMessage);
    }
}
