﻿using System.ServiceModel;

namespace PHRServiceWebRole
{
    /// <summary>
    /// The interface IFamilyMemberService defines the methods the service offers
    /// to manage user's family members.
    /// </summary>
    [ServiceContract]
    public interface IFamilyMemberService
    {
        /// <summary>
        /// This method returns a PersonArrayMessage which PersonArray value contains
        /// all the family members for the given user.
        /// </summary>
        /// <param name="idUser">The IdMessage containing the user Id</param>
        /// <returns>The PersonArrayMessage</returns>
        [OperationContract]
        PersonArrayMessage getAllFamilyMembers(IdMessage idUser);

        /// <summary>
        /// This method adds a family member into the database. It returns a BooleanMessage which boolean value 
        /// is true if the given family member has been succesfully added or false if an error occured.
        /// </summary>
        /// <param name="personMessage">The PersonMessage containing the family member to add</param>
        /// <returns>The BooleanMessage</returns>
        [OperationContract]
        BooleanMessage addFamilyMember(PersonMessage personMessage);

        /// <summary>
        /// This method removes a family member from the database. It returns a BooleanMessage which boolean value 
        /// is true if the given family member has been succesfully removed or false if an error occured.
        /// </summary>
        /// <param name="personMessage">The PersonMessage containing the family member to remove</param>
        /// <returns>The BooleanMessage</returns>
        [OperationContract]
        BooleanMessage removeFamilyMember(PersonMessage personMessage);

        /// <summary>
        /// This method edits a family member from the database. It returns a BooleanMessage which boolean value 
        /// is true if the given family member has been succesfully edited or false if an error occured.
        /// </summary>
        /// <param name="personMessage">The PersonMessage containing the family member to edit</param>
        /// <returns>The BooleanMessage</returns>
        [OperationContract]
        BooleanMessage editFamilyMember(PersonMessage personMessage); 
    }
}
