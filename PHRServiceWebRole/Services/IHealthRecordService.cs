﻿using System.ServiceModel;

namespace PHRServiceWebRole
{
    /// <summary>
    /// The interface IHealthRecordService defines the methods the service offers
    /// to manage user's health records.
    /// </summary>
    [ServiceContract]
    public interface IHealthRecordService
    {
        /// <summary>
        /// This method returns a HealthRecordArrayMessage which HealthRecordArray value contains
        /// all the health records for the given user.
        /// </summary>
        /// <param name="idUser">The IdMessage containing the user Id</param>
        /// <returns>The HealthRecordArrayMessage</returns>
        [OperationContract]
        HealthRecordArrayMessage getAllHealthRecords(IdMessage idUser);

        /// <summary>
        /// This method adds a health record into the database. It returns a BooleanMessage which boolean value 
        /// is true if the given health record has been succesfully added or false if an error occured.
        /// </summary>
        /// <param name="healthRecordMessage">The HealthRecordMessage containing the health record to add</param>
        /// <returns>The BooleanMessage</returns>
        [OperationContract]
        BooleanMessage addHealthRecord(HealthRecordMessage healthRecordMessage);

        /// <summary>
        /// This method removes a health record from the database. It returns a BooleanMessage which boolean value 
        /// is true if the given health record has been succesfully removed or false if an error occured.
        /// </summary>
        /// <param name="healthRecordMessage">The HealthRecordMessage containing the health record to remove</param>
        /// <returns>The BooleanMessage</returns>
        [OperationContract]
        BooleanMessage removeHealthRecord(HealthRecordMessage healthRecordMessage);
    }
}
