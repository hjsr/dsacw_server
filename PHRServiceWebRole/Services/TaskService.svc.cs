﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace PHRServiceWebRole
{
    public class TaskService : ITaskService
    {
        public TaskArrayMessage getAllTasks(IdMessage idUser)
        {
            SqlConnection conn = new SqlConnection("Data Source=kx1lehq9ru.database.windows.net;Initial Catalog=dsacwphrdatabase;Persist Security Info=True;User ID=nimda;Password=ABCabc123#");
            conn.Open();

            SqlCommand command = conn.CreateCommand();
            command.CommandText = "SELECT * FROM Task WHERE (IdUser = " + idUser.Id + ")";
            SqlDataReader reader = command.ExecuteReader();

            List<Task> taskList = new List<Task>();

            while (reader.Read())
            {
                Task task = new Task();
                task.Caption = reader["Caption"].ToString().Trim();
                if (!reader.IsDBNull(reader.GetOrdinal("Date")))
                {
                    task.Date = reader.GetDateTime(reader.GetOrdinal("Date"));
                } 
                task.Description = reader["Description"].ToString().Trim();
                task.Id = reader.GetInt32(reader.GetOrdinal("Id"));
                task.IdUser = reader.GetInt32(reader.GetOrdinal("IdUser"));
                task.Priority = (PriorityType)reader.GetInt32(reader.GetOrdinal("Priority"));
                task.Done = Convert.ToBoolean(reader.GetInt32(reader.GetOrdinal("Done")));
                taskList.Add(task);
            }

            conn.Close();

            TaskArrayMessage taskArrayMessage = new TaskArrayMessage();
            taskArrayMessage.Timestamp = DateTime.Now;
            taskArrayMessage.TaskArray = taskList.ToArray();
            return taskArrayMessage;
        }

        public BooleanMessage addTask(TaskMessage taskMessage)
        {
            SqlConnection conn = new SqlConnection("Data Source=kx1lehq9ru.database.windows.net;Initial Catalog=dsacwphrdatabase;Persist Security Info=True;User ID=nimda;Password=ABCabc123#");
            conn.Open();

            SqlCommand command = conn.CreateCommand();

            string commandTxt1 = "INSERT INTO Task (IdUser, Caption, Date, Priority, Description, Done) VALUES ("
                + taskMessage.Task.IdUser + ", N'"
                + taskMessage.Task.Caption;

            string commandTxt2;
            if (taskMessage.Task.Date == DateTime.MinValue)
            {
                commandTxt2 = "', NULL";
            }
            else
            {
                commandTxt2 = "', CONVERT(DATETIME, '"
                    + String.Format("{0:yyyy-M-d HH:mm:ss}", taskMessage.Task.Date) + "', 102)";
            }

            string commandTxt3 = ", "
                + (int)taskMessage.Task.Priority + ", N'"
                + taskMessage.Task.Description + "', 0)";

            command.CommandText = commandTxt1 + commandTxt2 + commandTxt3;

            BooleanMessage booleanMessage = new BooleanMessage();
            booleanMessage.Timestamp = DateTime.Now;
            booleanMessage.Value = command.ExecuteNonQuery() != 0;

            conn.Close();

            return booleanMessage;
        }

        public BooleanMessage removeTask(TaskMessage taskMessage)
        {
            SqlConnection conn = new SqlConnection("Data Source=kx1lehq9ru.database.windows.net;Initial Catalog=dsacwphrdatabase;Persist Security Info=True;User ID=nimda;Password=ABCabc123#");
            conn.Open();

            SqlCommand command = conn.CreateCommand();
            command.CommandText = "DELETE FROM Task WHERE (Id = " + taskMessage.Task.Id + ")";

            BooleanMessage booleanMessage = new BooleanMessage();
            booleanMessage.Timestamp = DateTime.Now;
            booleanMessage.Value = command.ExecuteNonQuery() != 0;

            conn.Close();

            return booleanMessage;
        }

        public BooleanMessage editTask(TaskMessage taskMessage)
        {
            SqlConnection conn = new SqlConnection("Data Source=kx1lehq9ru.database.windows.net;Initial Catalog=dsacwphrdatabase;Persist Security Info=True;User ID=nimda;Password=ABCabc123#");
            conn.Open();

            SqlCommand command = conn.CreateCommand();

            string commandTxt1 = "UPDATE Task SET Caption = N'"
                + taskMessage.Task.Caption;
            
            string commandTxt2;
            if (taskMessage.Task.Date == DateTime.MinValue)
            {            
                commandTxt2 = "', Date = NULL";
            }
            else
            {
                commandTxt2 = "', Date = CONVERT(DATETIME, '"
                    + String.Format("{0:yyyy-M-d HH:mm:ss}", taskMessage.Task.Date) + "', 102)";
            }

            string commandTxt3 = ", Description = N'"
                    + taskMessage.Task.Description + "', Priority = "
                    + (int)taskMessage.Task.Priority + ", Done = "
                    + Convert.ToInt32(taskMessage.Task.Done) + " WHERE (Id = "
                    + taskMessage.Task.Id + ")";

            command.CommandText = commandTxt1 + commandTxt2 + commandTxt3;

            BooleanMessage booleanMessage = new BooleanMessage();
            booleanMessage.Timestamp = DateTime.Now;
            booleanMessage.Value = command.ExecuteNonQuery() != 0;

            conn.Close();

            return booleanMessage;
        }
    }
}
