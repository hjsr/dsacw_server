﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace PHRServiceWebRole
{
    public class HealthRecordService : IHealthRecordService
    {
        public HealthRecordArrayMessage getAllHealthRecords(IdMessage idPerson)
        {
            SqlConnection conn = new SqlConnection("Data Source=kx1lehq9ru.database.windows.net;Initial Catalog=dsacwphrdatabase;Persist Security Info=True;User ID=nimda;Password=ABCabc123#");
            conn.Open();

            SqlCommand command = conn.CreateCommand();
            command.CommandText = "SELECT * FROM HealthRecord WHERE (IdPerson = " + idPerson.Id + ")";
            SqlDataReader reader = command.ExecuteReader();

            List<HealthRecord> healthRecordList = new List<HealthRecord>();

            while (reader.Read())
            {
                HealthRecord healthRecord = new HealthRecord();
                healthRecord.Caption = reader["Caption"].ToString().Trim();
                healthRecord.IdPerson = reader.GetInt32(reader.GetOrdinal("IdPerson"));
                healthRecord.Type = (HealthRecordType)reader.GetInt32(reader.GetOrdinal("Type"));
                healthRecord.Id = reader.GetInt32(reader.GetOrdinal("Id"));
                healthRecordList.Add(healthRecord);
            }

            conn.Close();

            HealthRecordArrayMessage healthRecordArrayMessage = new HealthRecordArrayMessage();
            healthRecordArrayMessage.Timestamp = DateTime.Now;
            healthRecordArrayMessage.HealthRecordArray = healthRecordList.ToArray();
            return healthRecordArrayMessage;
        }
       
        public BooleanMessage addHealthRecord(HealthRecordMessage healthRecordMessage)
        {
            SqlConnection conn = new SqlConnection("Data Source=kx1lehq9ru.database.windows.net;Initial Catalog=dsacwphrdatabase;Persist Security Info=True;User ID=nimda;Password=ABCabc123#");
            conn.Open();

            SqlCommand command = conn.CreateCommand();
            command.CommandText = "INSERT INTO HealthRecord (IdPerson, Caption, Type) VALUES ("
                + healthRecordMessage.HealthRecord.IdPerson + ", N'"
                + healthRecordMessage.HealthRecord.Caption + "', "
                + (int)healthRecordMessage.HealthRecord.Type + ")";

            BooleanMessage booleanMessage = new BooleanMessage();
            booleanMessage.Timestamp = DateTime.Now;
            booleanMessage.Value = command.ExecuteNonQuery() != 0;

            conn.Close();

            return booleanMessage;
        }

        public BooleanMessage removeHealthRecord(HealthRecordMessage healthRecordMessage)
        {
            SqlConnection conn = new SqlConnection("Data Source=kx1lehq9ru.database.windows.net;Initial Catalog=dsacwphrdatabase;Persist Security Info=True;User ID=nimda;Password=ABCabc123#");
            conn.Open();

            SqlCommand command = conn.CreateCommand();
            command.CommandText = "DELETE FROM HealthRecord WHERE (Id = " + healthRecordMessage.HealthRecord.Id + ")";

            BooleanMessage booleanMessage = new BooleanMessage();
            booleanMessage.Timestamp = DateTime.Now;
            booleanMessage.Value = command.ExecuteNonQuery() != 0;

            conn.Close();

            return booleanMessage;
        }
    }
}
