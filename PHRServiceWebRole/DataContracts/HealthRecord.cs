﻿using System;
using System.Runtime.Serialization;

namespace PHRServiceWebRole
{
    /// <summary>
    /// The enum class HealthRecordType defines the type of
    /// document that a health record could represent.
    /// </summary>
    [DataContract]
    public enum HealthRecordType
    {
        [EnumMember]
        ALLERGIE,
        [EnumMember]
        BLOOD_PRESSURE,
        [EnumMember]
        FAMILY_HISTORY,
        [EnumMember]
        ILLNESS,
        [EnumMember]
        IMAGE,
        [EnumMember]
        IMMUNIZATION,
        [EnumMember]
        LAB_RESULT,
        [EnumMember]
        MEDICATION
    };

    /// <summary>
    /// The class HealthRecord defines a health record that the user
    /// added.
    /// <para>The class HealthRecord has 4 attributes:</para>
    /// <list type="Attributes">
    /// <item>string Caption: The caption</item>
    /// <item>int Id: The identifier</item>
    /// <item>int IdUser: The user's identifier who has the health record</item>
    /// <item>HealthRecordType Type: The type</item>
    /// </list>
    /// </summary>
    [DataContract]
    public class HealthRecord
    {
        [DataMember]
        public string Caption { get; set; }

        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int IdPerson { get; set; }

        [DataMember]
        public HealthRecordType Type { get; set; }
    }
}