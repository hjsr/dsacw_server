﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace PHRServiceWebRole
{
    /// <summary>
    /// The class Goal defines a goal that the user wants
    /// to achive.
    /// <para>The class Goal has 5 attributes:</para>
    /// <list type="Attributes">
    /// <item>string Caption: The caption</item>
    /// <item>int CurrentProgress: The current progress as a percentage</item>
    /// <item>string Description: The description</item>
    /// <item>int Id: The identifier</item>
    /// <item>int IdUser: The user's identifier who has the goal</item>
    /// </list>
    /// </summary>
    [DataContract]
    public class Goal
    {
        [DataMember]
        public string Caption { get; set; }

        [DataMember]
        public int CurrentProgress { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int IdUser { get; set; }
    }
}