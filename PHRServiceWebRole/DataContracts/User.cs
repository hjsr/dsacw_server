﻿using System;
using System.Runtime.Serialization;

namespace PHRServiceWebRole
{
    /// <summary>
    /// The class User defines a person that can use the application.
    /// <para>The class User extends the class Person</para>
    /// <para>The class User has 2 more attributes than Person:</para>
    /// <list type="Attributes">
    /// <item>string Username: The username</item>
    /// <item>string Password: The password</item>
    /// </list>
    /// </summary>
    [DataContract]
    public class User : Person
    {
        [DataMember]
        public string Password { get; set; }

        [DataMember]
        public string Username { get; set; }
    }
}