﻿using System;
using System.Runtime.Serialization;

namespace PHRServiceWebRole
{
    /// <summary>
    /// The enum class PriorityType defines the priority of
    /// a task.
    /// </summary>
    [DataContract]
    public enum PriorityType 
    {
        [EnumMember]
        LOW, 
        [EnumMember]
        NORMAL,
        [EnumMember]
        HIGH
    };

    /// <summary>
    /// The class Task defines a task.
    /// <para>The class Task has 7 attributes:</para>
    /// <list type="Attributes">
    /// <item>string Caption: The caption</item>
    /// <item>DateTime Date: The date when the task happens</item>
    /// <item>string Description: The description</item>
    /// <item>int Id: The identifier</item>
    /// <item>int IdUser: The user's identifier who has the task</item>
    /// <item>PriorityType Priority: The priority</item>
    /// <item>bool Done: Boolean value which defines if the task has been done or no</item>
    /// </list>
    /// </summary>
    [DataContract]
    public class Task
    {
        [DataMember]
        public string Caption { get; set; }

        [DataMember]
        public DateTime Date { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int IdUser { get; set; }

        [DataMember]
        public PriorityType Priority { get; set; }

        [DataMember]
        public bool Done { get; set; }
    }
}