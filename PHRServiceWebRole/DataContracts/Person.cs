﻿using System;
using System.Runtime.Serialization;

namespace PHRServiceWebRole
{
    /// <summary>
    /// The enum class Gender defines the gender of
    /// a person.
    /// </summary>
    [DataContract]
    public enum Gender
    {
        [EnumMember]
        FEMALE,
        [EnumMember]
        MALE
    };

    /// <summary>
    /// The class Person defines a person.
    /// <para>In this application, a person represents a family member or a user</para>
    /// <para>The class Person has 7 attributes:</para>
    /// <list type="Attributes">
    /// <item>string Address: The address</item>
    /// <item>DateTime DateOfBirth: The date of birth</item>
    /// <item>string Email: The email</item>
    /// <item>Gender: The gender (MALE or FEMALE)</item>
    /// <item>int Id: The identifier</item>
    /// <item>int IdUser: The user's identifier who the person is rattached to</item>
    /// <item>string Name: The name</item>
    /// </list>
    /// </summary>
    [DataContract]
    public class Person
    {
        [DataMember]
        public string Address { get; set; }

        [DataMember]
        public DateTime DateOfBirth { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public Gender Gender { get; set; }

        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int IdUser { get; set; }

        [DataMember]
        public string Name { get; set; }
    }
}