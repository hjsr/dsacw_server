﻿using System;
using System.ServiceModel;

namespace PHRServiceWebRole
{
    /// <summary>
    /// The class PersonArrayMessage defines a SOAP message containing an array of Person.
    /// <para>The class PersonArrayMessage has 2 attributes:</para>
    /// <list type="Attributes">
    /// <item>DateTime Timestamp: The datetime the the soap message has been created</item>
    /// <item>bool PersonArray: The array of persons</item>
    /// </list>
    /// </summary>
    [MessageContract]
    public class PersonArrayMessage
    {
        [MessageHeader]
        public DateTime Timestamp { get; set; }

        [MessageBodyMember]
        public Person[] PersonArray { get; set; }
    }
}