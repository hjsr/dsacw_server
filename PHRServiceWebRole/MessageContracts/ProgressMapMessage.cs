﻿using System;
using System.ServiceModel;
using System.Collections.Generic;

namespace PHRServiceWebRole
{
    /// <summary>
    /// The class ProgressMapMessage defines a SOAP message containing a Map of Progress.
    /// <para>The class ProgressMapMessage has 2 attributes:</para>
    /// <list type="Attributes">
    /// <item>DateTime Timestamp: The datetime the the soap message has been created</item>
    /// <item>bool ProgressMap: The map of progress</item>
    /// </list>
    /// </summary>
    [MessageContract]
    public class ProgressMapMessage
    {
        [MessageHeader]
        public DateTime Timestamp { get; set; }

        [MessageBodyMember]
        public Dictionary<DateTime, int> ProgressMap { get; set; }
    }
}