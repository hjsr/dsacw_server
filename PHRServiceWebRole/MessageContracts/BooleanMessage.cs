﻿using System;
using System.ServiceModel;

namespace PHRServiceWebRole
{
    /// <summary>
    /// The class BooleanMessage defines a SOAP message containing a boolean value.
    /// <para>The class BooleanMessage has 2 attributes:</para>
    /// <list type="Attributes">
    /// <item>DateTime Timestamp: The datetime the the soap message has been created</item>
    /// <item>bool Value: The boolean value</item>
    /// </list>
    /// </summary>
    [MessageContract]
    public class BooleanMessage
    {
        [MessageHeader]
        public DateTime Timestamp { get; set; }

        [MessageBodyMember]
        public bool Value { get; set; }
    }
}