﻿using System;
using System.ServiceModel;

namespace PHRServiceWebRole
{
    /// <summary>
    /// The class UserMessage defines a SOAP message containing a User.
    /// <para>The class UserMessage has 2 attributes:</para>
    /// <list type="Attributes">
    /// <item>DateTime Timestamp: The datetime the the soap message has been created</item>
    /// <item>bool User: The user</item>
    /// </list>
    /// </summary>
    [MessageContract]
    public class UserMessage
    {
        [MessageHeader]
        public DateTime Timestamp { get; set; }

        [MessageBodyMember]
        public User User { get; set; }
    }
}