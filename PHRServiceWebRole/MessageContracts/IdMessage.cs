﻿using System;
using System.ServiceModel;

namespace PHRServiceWebRole
{
    /// <summary>
    /// The class IdMessage defines a SOAP message containing an int value.
    /// <para>The class IdMessage has 2 attributes:</para>
    /// <list type="Attributes">
    /// <item>DateTime Timestamp: The datetime the the soap message has been created</item>
    /// <item>int Id: The int value</item>
    /// </list>
    /// </summary>
    [MessageContract]
    public class IdMessage
    {
        [MessageHeader]
        public DateTime Timestamp { get; set; }

        [MessageBodyMember]
        public int Id { get; set; }
    }
}