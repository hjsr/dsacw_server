﻿using System;
using System.ServiceModel;

namespace PHRServiceWebRole
{
    /// <summary>
    /// The class GoalMessage defines a SOAP message containing a Goal.
    /// <para>The class GoalMessage has 2 attributes:</para>
    /// <list type="Attributes">
    /// <item>DateTime Timestamp: The datetime the the soap message has been created</item>
    /// <item>bool Goal: The goal</item>
    /// </list>
    /// </summary>
    [MessageContract]
    public class GoalMessage
    {
        [MessageHeader]
        public DateTime Timestamp { get; set; }

        [MessageBodyMember]
        public Goal Goal { get; set; }
    }
}