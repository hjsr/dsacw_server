﻿using System;
using System.ServiceModel;

namespace PHRServiceWebRole
{
    /// <summary>
    /// The class TaskArrayMessage defines a SOAP message containing an array of Task.
    /// <para>The class TaskArrayMessage has 2 attributes:</para>
    /// <list type="Attributes">
    /// <item>DateTime Timestamp: The datetime the the soap message has been created</item>
    /// <item>bool TaskArray: The array of tasks</item>
    /// </list>
    /// </summary>
    [MessageContract]
    public class TaskArrayMessage
    {
        [MessageHeader]
        public DateTime Timestamp { get; set; }

        [MessageBodyMember]
        public Task[] TaskArray { get; set; }
    }
}