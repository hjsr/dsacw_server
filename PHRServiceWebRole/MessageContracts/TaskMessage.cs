﻿using System;
using System.ServiceModel;

namespace PHRServiceWebRole
{
    /// <summary>
    /// The class TaskMessage defines a SOAP message containing a Task.
    /// <para>The class TaskMessage has 2 attributes:</para>
    /// <list type="Attributes">
    /// <item>DateTime Timestamp: The datetime the the soap message has been created</item>
    /// <item>bool Task: The task</item>
    /// </list>
    /// </summary>
    [MessageContract]
    public class TaskMessage
    {
        [MessageHeader]
        public DateTime Timestamp { get; set; }

        [MessageBodyMember]
        public Task Task { get; set; }
    }
}