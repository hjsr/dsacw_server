﻿using System;
using System.ServiceModel;

namespace PHRServiceWebRole
{
    /// <summary>
    /// The class PersonMessage defines a SOAP message containing a Person.
    /// <para>The class PersonMessage has 2 attributes:</para>
    /// <list type="Attributes">
    /// <item>DateTime Timestamp: The datetime the the soap message has been created</item>
    /// <item>bool Person: The person</item>
    /// </list>
    /// </summary>
    [MessageContract]
    public class PersonMessage
    {
        [MessageHeader]
        public DateTime Timestamp { get; set; }

        [MessageBodyMember]
        public Person Person { get; set; }
    }
}