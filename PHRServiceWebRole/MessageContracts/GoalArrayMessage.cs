﻿using System;
using System.ServiceModel;

namespace PHRServiceWebRole
{
    /// <summary>
    /// The class GoalArrayMessage defines a SOAP message containing an array of Goal.
    /// <para>The class GoalArrayMessage has 2 attributes:</para>
    /// <list type="Attributes">
    /// <item>DateTime Timestamp: The datetime the the soap message has been created</item>
    /// <item>bool GoalArray: The array of goals</item>
    /// </list>
    /// </summary>
    [MessageContract]
    public class GoalArrayMessage
    {
        [MessageHeader]
        public DateTime Timestamp { get; set; }

        [MessageBodyMember]
        public Goal[] GoalArray { get; set; }
    }
}