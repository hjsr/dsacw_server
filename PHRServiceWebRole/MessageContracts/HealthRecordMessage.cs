﻿using System;
using System.ServiceModel;

namespace PHRServiceWebRole
{
    /// <summary>
    /// The class HealthRecordMessage defines a SOAP message containing a HealthRecord.
    /// <para>The class HealthRecordMessage has 2 attributes:</para>
    /// <list type="Attributes">
    /// <item>DateTime Timestamp: The datetime the the soap message has been created</item>
    /// <item>bool HealthRecord: The health record</item>
    /// </list>
    /// </summary>
    [MessageContract]
    public class HealthRecordMessage
    {
        [MessageHeader]
        public DateTime Timestamp { get; set; }

        [MessageBodyMember]
        public HealthRecord HealthRecord { get; set; }
    }
}