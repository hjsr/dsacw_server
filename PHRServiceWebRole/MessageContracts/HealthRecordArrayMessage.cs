﻿using System;
using System.ServiceModel;

namespace PHRServiceWebRole
{
    /// <summary>
    /// The class HealthRecordArrayMessage defines a SOAP message containing an array of HealthRecord.
    /// <para>The class HealthRecordArrayMessage has 2 attributes:</para>
    /// <list type="Attributes">
    /// <item>DateTime Timestamp: The datetime the the soap message has been created</item>
    /// <item>bool HealthRecordArray: The array of health records</item>
    /// </list>
    /// </summary>
    [MessageContract]
    public class HealthRecordArrayMessage
    {
        [MessageHeader]
        public DateTime Timestamp { get; set; }

        [MessageBodyMember]
        public HealthRecord[] HealthRecordArray { get; set; }
    }
}